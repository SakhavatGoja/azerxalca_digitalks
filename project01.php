<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'code_here', 'auto');
		ga('send', 'pageview');
	</script>
    
    <title>Rayden - Creative Showcase Portfolio Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Download the best Creative Portfolio HTML Template in 2020" />
    <meta name="author" content="ClaPat Studio">
    <meta charset="UTF-8" />    
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
   
</head>
    


<body class="hidden hidden-ball smooth-scroll" data-primary-color="#E7B970">

	
	<main>		
        <!-- Preloader -->
        <div class="preloader-wrap" data-firstline="Stay" data-secondline="Relaxed">
            <div class="outer">
                <div class="inner">                    
                    <div class="trackbar">
                    	<div class="preloader-intro">Please Wait</div>
                        <div class="loadbar"></div>
                    </div>
                    <div class="percentage-intro">Loaded</div>
                    <div class="percentage-wrapper"><div class="percentage" id="precent"></div></div>                     
                </div>
            </div>
        </div>
        <!--/Preloader -->    
        
        <div class="cd-index cd-main-content">
    
        <!-- Page Content -->
        <div id="page-content" class="light-content layered-hero" data-bgcolor="#141414">
            
        <?php include 'includes/header.php';?>       
            
            
             
            
            
            <!-- Content Scroll -->
            <div id="content-scroll">
            
            
                <!-- Main -->
                <div id="main">
                
                    <!-- Hero Section -->
                    <div id="hero" class="has-image autoscroll1">
                        <div id="hero-styles">
                            <div id="hero-caption" class="reverse-parallax-onscroll">
                                <div class="inner">
                                    <div class="hero-title"><span>Sheeran</span></div>                                    
                                    <div class="hero-subtitle"><span>Branding</span></div>
                                </div>
                            </div>
                            <div id="hero-footer">
                            	<div class="hero-footer-left">
                                	<div class="button-wrap left disable-drag scroll-down">
                                        <div class="icon-wrap parallax-wrap">
                                            <div class="button-icon parallax-element">
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                        </div>
                                        <div class="button-text sticky left"><span data-hover="Scroll or drag to navigate">Scroll or drag to navigate</span></div> 
                                    </div>
                                </div>
                                <div class="hero-footer-right">
                    				<div id="share" class="page-action-content disable-drag" data-text="Share:"></div>
                                </div>
                            </div>                                    
                        </div>
                    </div>
                    <div id="hero-image-wrapper">
                    	<div id="hero-background-layer" class="parallax-scroll-effect">
                            <div id="hero-bg-image" style="background-image:url(images/01hero.jpg)"></div>
                        </div>
                    </div>                        
                    <!--/Hero Section -->   
                         
                    
                    <!-- Main Content -->
                    <div id="main-content">
                            <!-- Row -->
                            <!-- <div class="vc_row row_padding_top row_padding_bottom dark-section">
                                
                            </div> -->
                            <div class="experimental_header">
                                <div class="tabs">
                                    <a href="#" class="ex_link">Modern</a>
                                    <a href="#" class="ex_link active">Abstract</a>
                                    <a href="#" class="ex_link">Young authors</a>
                                </div>
                                <div class="mobile_tabs_container">
                                    <div class="tabs_active_show">
                                        <p class="active_mobile_tab">Modern</p>
                                        <img src="images/arrow_tabs.svg" alt="">
                                    </div>
                                    <div class="mobile_tabs">
                                        <a href="#">Modern</a>
                                        <a href="#">Abstract</a>
                                        <a href="#">Young authors</a>
                                    </div>
                                </div>
                                <div class="ex_filter">
                                    <button class="title_filter">Filter</button>
                                    <div class="filter-box">
                                        <div class="filter-property">
                                            <div class="filter-header">Size<img src="images/arrow_filter.svg" alt=""></div>
                                            <ul class="filter-checks">
                                                <li>
                                                    <input id="c1" type="checkbox" name="checkbox">
                                                    <label for="c1">Checkbox</label>
                                                </li>
                                                <li>
                                                    <input id="c2" type="checkbox" checked>
                                                    <label for="c2">Checkbox</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="filter-property">
                                            <div class="filter-header">Type<img src="images/arrow_filter.svg" alt=""></div>
                                        </div>
                                        <div class="filter-property">
                                            <div class="filter-header">Color<img src="images/arrow_filter.svg" alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carpets_grid">
                                <div class="carpet_single">
                                    <a href="carpet_inner.php" class="carpet_img"><img src="images/carpet_1.jpg" alt=""></a>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <a href="carpet_inner.php" class="carpet_img"><img src="images/carpet_2.jpg" alt=""></a>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <a href="carpet_inner.php" class="carpet_img"><img src="images/carpet_3.jpg" alt=""></a>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <a href="carpet_inner.php" class="carpet_img"><img src="images/carpet_4.jpg" alt=""></a>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <a href="carpet_inner.php" class="carpet_img"><img src="images/carpet_5.jpg" alt=""></a>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <a href="carpet_inner.php" class="carpet_img"><img src="images/carpet_6.jpg" alt=""></a>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                            </div>   
                        <!--/Main Page Content --> 
                    </div>
                    <!--/Main Content --> 
                </div>
                <!--/Main --> 
                
                
                <?php include 'includes/footer.php';?>
            
        
        	</div>
            <!--/Content Scroll -->
            
            <div id="app"></div>
           
            <div class="next-project-image-wrapper">
                <div class="next-project-image">
                    <div class="next-project-image-bg" style="background-image:url(images/02hero.jpg)"></div>
                </div>            
            </div>
            
		</div>    
        <!--/Page Content -->
    
		</div>
	</main>
    
    
    
   <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-drag-x"></div>
            <div id="ball-drag-y"></div>
        	<div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>
    
    
		
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js" ></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfqCBbOT1SDziLIysLIVMUQbKHJFqrlEE&sensor=false"></script>
	<script src="js/clapatwebgl.js"></script>
	<script src="js/plugins.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/custom.js"></script>



</body>

</html>