<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-164063138-1', 'auto');
		ga('send', 'pageview');
	</script>
    
    <title>Rayden - Creative Showcase Portfolio Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Download the best Creative Portfolio HTML Template in 2020" />
    <meta name="author" content="ClaPat Studio">
    <meta charset="UTF-8" />    
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    
   
</head>


<body class="hidden hidden-ball smooth-scroll" data-primary-color="#E7B970">
		<!-- sdsd -->
	
	<main>		
        <!-- Preloader -->
        <div class="preloader-wrap" data-firstline="Stay" data-secondline="Relaxed">
            <div class="outer">
                <div class="inner">                    
                    <div class="trackbar">
                    	<div class="preloader-intro">Please Wait</div>
                        <div class="loadbar"></div>
                    </div>
                    <div class="percentage-intro">Loaded</div>
                    <div class="percentage-wrapper"><div class="percentage" id="precent"></div></div>                     
                </div>
            </div>
        </div>
        <!--/Preloader -->    
        
        <div class="cd-index cd-main-content">
    
        <!-- Page Content -->
        <div id="page-content" class="light-content" data-bgcolor="#141414">
            
            <?php include 'includes/header.php';?> 
            
           
            
            <!-- Content Scroll -->
            <div id="content-scroll">
                        
                                                        
                <!-- Showcase Slider Holder -->
                <div id="showcase-slider-webgl-holder" class="disable-drag" data-pattern-img="images/displacement/1.jpg">
                    <div id="slider-webgl" class="swiper-container">
                        <div id="trigger-slides" class="swiper-wrapper">
                        
                            <div class="swiper-slide">
                                <div class="slide-wrap active" data-slide="0"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="" data-firstline="View" data-secondline="Project"><span>Sheeran</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Branding</span></div>
                                    </div>
                                 </div>
                            </div>
                            
                            <div class="swiper-slide" data-swiper-parallax-opacity="0">
                                <div class="slide-wrap" data-slide="1"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                         <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="" data-firstline="View" data-secondline="Project"><span>The Rituals</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Design</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="2"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/03hero.jpg" data-firstline="View" data-secondline="Project"><span>Ocean Waves</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Photography</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="3"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/04hero.jpg" data-firstline="View" data-secondline="Project"><span>Forest</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Design</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="4"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/05hero.jpg" data-firstline="View" data-secondline="Project"><span>Delmia Girl</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Photography</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="5"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/06hero.jpg" data-firstline="View" data-secondline="Project"><span>Gold Man</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Video</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="6"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/07hero.jpg" data-firstline="View" data-secondline="Project"><span>Burkhard</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Design</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="7"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/08hero.jpg" data-firstline="View" data-secondline="Project"><span>Modern Food</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Photography</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="swiper-slide">
                                <div class="slide-wrap" data-slide="8"></div>
                                <div class="outer" data-swiper-parallax="0" data-swiper-parallax-opacity="0.1">
                                    <div class="inner">
                                        <div class="slide-title-wrapper">
                                            <div class="slide-title" data-fill-img="images/08hero.jpg" data-firstline="View" data-secondline="Project"><span>Loft Studio</span></div>
                                            <a class="slide-link" data-type="page-transition" href="project01.php"></a>
                                        </div>
                                        <div class="subtitle" data-swiper-parallax-opacity="0"><span>Design</span></div>
                                    </div>
                                </div>
                            </div>
                            
                       </div>
                   </div>                            
                </div>    
                <!-- Showcase Slider Holder -->
                
                
                
                <!-- canvas slider -->
                <div id="canvas-slider" class="canvas-slider">                
                    <div class="slider-img" data-slide="0">
                        <img class="slide-img" src="images/xalca1.jpg" />
                    </div>
                    <div class="slider-img" data-slide="1">
                        <img class="slide-img" src="images/background.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="2">
                        <img class="slide-img" src="images/xalca1.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="3">
                        <img class="slide-img" src="images/background.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="4">
                        <img class="slide-img" src="images/xalca1.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="5">
                        <img class="slide-img" src="images/background.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="6">
                        <img class="slide-img" src="images/xalca1.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="7">
                        <img class="slide-img" src="images/background.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                    <div class="slider-img" data-slide="8">
                        <img class="slide-img" src="images/xalca1.jpg" />
                        <div class="layer_gray"></div>
                    </div>
                </div>
                <!--/canvas slider -->

                <?php include 'includes/footer.php';?> 
                            
            </div>
            <!--/Content Scroll -->        
					                            
                
            
            
        
        	
            
            
            <div id="app"></div>
            
            
		</div>    
        <!--/Page Content -->
    
		</div>
	</main>
    
    
    
    
    <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-drag-x"></div>
            <div id="ball-drag-y"></div>
        	<div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>
    
    
		
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js" ></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpK1sWi3J3EbUOkF_K4-UHzi285HyFX5M&sensor=false"></script>
	<script src="js/clapatwebgl.js"></script>
	<script src="js/plugins.js"></script>
    <script src="js/scripts.js"></script>



</body>

</html>