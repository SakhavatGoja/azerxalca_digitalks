<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'code_here', 'auto');
		ga('send', 'pageview');
	</script>
    
    <title>Rayden - Creative Showcase Portfolio Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Download the best Creative Portfolio HTML Template in 2020" />
    <meta name="author" content="ClaPat Studio">
    <meta charset="UTF-8" />    
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
   
</head>
    

<body class="hidden hidden-ball smooth-scroll" data-primary-color="#E7B970">

	
	<main>		
        <!-- Preloader -->
        <div class="preloader-wrap" data-firstline="Stay" data-secondline="Relaxed">
            <div class="outer">
                <div class="inner">                    
                    <div class="trackbar">
                    	<div class="preloader-intro">Please Wait</div>
                        <div class="loadbar"></div>
                    </div>
                    <div class="percentage-intro">Loaded</div>
                    <div class="percentage-wrapper"><div class="percentage" id="precent"></div></div>                     
                </div>
            </div>
        </div>
        <!--/Preloader -->    
        
        <div class="cd-index cd-main-content">
    
        <!-- Page Content -->
        <div id="page-content" class="light-content" data-bgcolor="#141414">
            
        <?php include 'includes/header.php';?>     
            
            
            <!-- Content Scroll -->
            <div id="content-scroll">
            
            
                <!-- Main -->
                <div id="main">
                
                    <!-- Hero Section -->
                    <div id="hero">
                        <div id="hero-styles">
                            <div id="hero-caption" class="parallax-onscroll text-align-center">
                                <div class="inner">
                                	<h1 class="hero-title"><span>Contact Us</span></h1>
                                    <h5 class="hero-subtitle"><span>Let's work together</span></h5>                                                                  
                                </div>
                            </div>                                     
                        </div>
                    </div>                      
                    <!--/Hero Section -->    
                         
                    
                    <!-- Main Content -->
                    <div id="main-content" class="shops">
                        <!-- Main Page Content -->
                        <div id="main-page-content">
                            
                            
                            <!-- Row --> 
                            <div class="vc_row">
                                
                                <div class="row-half-color first half-dark-section" data-bgcolor="#141414"></div>
                                <div class="row-half-color second half-white-section change-header-color" data-bgcolor="#fff"></div>
                                                        
                                <div id="map_main"></div>
                            
                            </div>
                            <!--/Row -->
                            
                            
                            <!-- Row --> 
                            <div class="vc_row row_padding_top row_padding_bottom white-section text-align-center small change-header-color" data-bgcolor="#fff">
                                                        
                                <h2 class="has-animation" data-delay="0">Let's work together and make <br class="destroy">something that matters.</h2>
                                
                                <hr><hr>
                                    
                                <div class="one_third has-animation"  data-delay="100">
                                    <div class="clapat-icon">
                                        <i class="fa fa-paper-plane fa-2x" aria-hidden="true"></i>
                                    </div>
                                
                                    <h5 class="secondary-font"><a href="mailto:office@rayden.com" class="link"><span>office@rayden.com</span></a></h5>
                                    <p>Email</p>                        
                                </div>
                                
                                <div class="one_third has-animation"  data-delay="200">
                                    <div class="clapat-icon">
                                        <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                                    </div>
                                
                                    <h5 class="secondary-font">35 M Str, New York, USA</h5>
                                    <p>Address</p>                        
                                </div>
                                
                                <div class=" one_third last has-animation"  data-delay="300">
                                    <div class="clapat-icon">
                                        <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                    </div>
                                
                                    <h5 class="secondary-font">0040 (7763) 574-8901</h5>
                                    <p>Phone</p>
                                </div>
                            </div>
                            <div class="vc_row row_padding_bottom white-section change-header-color change-accordion" data-bgcolor="#fff">
                               <p class="title-shops">
                                Emalatkhana
                               </p>
                                <dl class="accordion white-section has-animation accordion-map">
                                        <dt>
                                            <span class="link">Horadiz carpet shop</span>
                                            <div class="acc-icon-wrap parallax-wrap">
                                                <div class="acc-button-icon parallax-element">
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </dt>
                                        <dd class="accordion-content">
                                            <div class="grid-accordion-block">
                                                <div class="all-info-labels">
                                                    <div>Opening date:<span>18.12.2018</span></div>
                                                    <div>Area:<span>1259 kv.m</span></div>
                                                    <div>The numbers of employees:<span>48(29 weaver , 9 technical staff)</span></div>
                                                    <div>Carpets with period of activity:<span>325</span></div>
                                                    <div>Touching is preferred:<span> Baku group Azerbaijan carpets</span></div>
                                                    <div>Address:<span>Sabunchu , Nardaran Settlement</span></div>
                                                    <div>Tel:<span>(050) 887-12-77</span></div>
                                                </div>
                                                <input type="hidden" name="" class="hidden" data-lat="40.41330695059439" data-lng="49.8184880463022">
                                                <div class="map">
                                                    <div id="map"></div>
                                                </div>
                                            </div>
                                        </dd>
    
                                        <dt>
                                            <span class="link">Shamkir carpet shop</span>
                                            <div class="acc-icon-wrap parallax-wrap">
                                                <div class="acc-button-icon parallax-element">
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </dt>
                                        <dd class="accordion-content">
                                            <div class="grid-accordion-block">
                                                <div class="all-info-labels">
                                                    <div>Opening date:<span>18.12.2018</span></div>
                                                    <div>Area:<span>1259 kv.m</span></div>
                                                    <div>The numbers of employees:<span>48(29 weaver , 9 technical staff)</span></div>
                                                    <div>Carpets with period of activity:<span>325</span></div>
                                                    <div>Touching is preferred:<span> Baku group Azerbaijan carpets</span></div>
                                                    <div>Address:<span>Sabunchu , Nardaran Settlement</span></div>
                                                    <div>Tel:<span>(050) 887-12-77</span></div>
                                                </div>
                                                <input type="hidden" name="" class="hidden" data-lat="40.382099955836814" data-lng="49.923137521004236">
                                                <div class="map"></div>
                                            </div>
                                        </dd> 
    
                                        <dt>
                                            <span class="link">Quba carpet shop</span>
                                            <div class="acc-icon-wrap parallax-wrap">
                                                <div class="acc-button-icon parallax-element">
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </dt>
                                        <dd class="accordion-content">
                                            <div class="grid-accordion-block">
                                                <div class="all-info-labels">
                                                    <div>Opening date:<span>18.12.2018</span></div>
                                                    <div>Area:<span>1259 kv.m</span></div>
                                                    <div>The numbers of employees:<span>48(29 weaver , 9 technical staff)</span></div>
                                                    <div>Carpets with period of activity:<span>325</span></div>
                                                    <div>Touching is preferred:<span> Baku group Azerbaijan carpets</span></div>
                                                    <div>Address:<span>Sabunchu , Nardaran Settlement</span></div>
                                                    <div>Tel:<span>(050) 887-12-77</span></div>
                                                </div>
                                                <input type="hidden" name="" class="hidden" data-lat="40.42794504418814" data-lng="49.9858927600059">
                                                <div class="map"></div>
                                            </div>
                                        </dd> 
                                </dl> 
                            </div>
                            <!-- Row -->
                            <div class="vc_row row_padding_top text-align-center">
                                
                                <h5 class="has-mask primary-color secondary-font">Get in Touch</h5>
                                <h2 class="has-animation" data-delay="150">Find out if I’m available and <br class="destroy">ask me anything</h2>
                                
                                <hr>
                                    
                                <!-- Contact Formular -->
                                <div id="contact-formular">
                                
                                    <div id="message"></div>
                                
                                    <form method="post" action="contact.php" class="contactform" name="contactform" id="contactform">                             
                                        <div class="name-box has-animation" data-delay="100">        
                                            <input name="name" type="text" id="name" size="30" value="" placeholder="What's Your Name"><label class="input_label"></label>
                                        </div>                                                        
                                        <div class="email-box has-animation" data-delay="150">
                                            <input name="email" type="text" id="email" size="30" value="" placeholder="Your Email"><label class="input_label"></label>
                                        </div>                            
                                        <div class="message-box has-animation" data-delay="100">
                                            <textarea name="comments" cols="40" rows="4" id="comments" placeholder="Tell Us About Your Project" ></textarea><label class="input_label slow"></label>
                                        </div>                             
                                        <div class="button-box has-animation" data-delay="100">             
                                            <div class="clapat-button-wrap circle parallax-wrap bigger">
                                                <div class="clapat-button parallax-element">
                                                	<div class="button-border  outline parallax-element-second"><input type="submit" class="send_message" id="submit" value="Submit" /></div>
                                                </div>
                                            </div> 
                                        </div>
                                    </form>                        
                                                            
                                </div>
                                <!--/Contact Formular -->
                           </div> 
                           <!--/Row -->
                            <!--/Row -->
                            
                            
                            
                            <!-- Row -->
                            
                           <!--/Row -->
                        </div>
                        <!--/Main Page Content -->
                    </div>
                    <!--/Main Content --> 
                
                </div>
                <!--/Main -->
                
                <?php include 'includes/footer.php';?>
            
        
        	</div>
            <!--/Content Scroll -->
            
            
            <div id="app"></div>
            
            <!-- Project Holder -->
            <div id="project-holder">
                <div id="project-data"></div>
            </div>
        	<!--/Project Holder -->

		</div>    
        <!--/Page Content -->
    
		</div>
	</main>
    
    
    
    
    <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-drag-x"></div>
            <div id="ball-drag-y"></div>
        	<div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>
    
    
		
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js" ></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4SgjsTpN_D4nuu-O2Dn8f5aeK7dLjwoQ&callback=initMap&libraries=places" async defer></script>
	<script src="js/clapatwebgl.js"></script>
	<script src="js/plugins.js"></script>
    <script src="js/scripts.js"></script>
    <!-- <script src="js/map.js"></script> -->
    <script>
        function initMap(){  
            var companyImage = new google.maps.MarkerImage('images/marker.png',
				new google.maps.Size(58,63),
				new google.maps.Point(0,0),
				new google.maps.Point(35,20)
			); 
            var CustomMapStyles = [
              {
                  "featureType": "all",
                  "elementType": "labels.text.stroke",
                  "stylers": [
                      {
                          "visibility": "on"
                      },
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 12
                      }
                  ]
              },
              {
                  "featureType": "all",
                  "elementType": "labels.icon",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "administrative",
                  "elementType": "geometry.fill",
                  "stylers": [
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 20
                      }
                  ]
              },
              {
                  "featureType": "administrative",
                  "elementType": "geometry.stroke",
                  "stylers": [
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 17
                      },
                      {
                          "weight": 1.2
                      }
                  ]
              },
              {
                  "featureType": "landscape",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 20
                      }
                  ]
              },
              {
                  "featureType": "poi",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "geometry.fill",
                  "stylers": [
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 17
                      }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "geometry.stroke",
                  "stylers": [
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 29
                      },
                      {
                          "weight": 0.2
                      }
                  ]
              },
              {
                  "featureType": "road.arterial",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    },
                    {
                    "weight":1
                    }
                  ]
              },
              {
                  "featureType": "road.local",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#00000"
                      },
                      {
                          "lightness": 16
                      },
                      {
                          "weight":2
                      }
                  ]
              },
              {
                  "featureType": "transit",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#00000"
                      },
                      {
                          "lightness": 19
                      },
                      {
                        "weight":2
                      }
                      
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#000000"
                      },
                      {
                          "lightness": 17
                      }
                  ]
              }
            ]; 
            var options = {
                zoom :14,
                center:{lat:43.257359,lng:6.656102},
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles:CustomMapStyles,
            }   
            var sabail = {lat:43.257359,lng:6.656102}
            var map  = new google.maps.Map(document.getElementById("map_main"),options);     
            var map1  = new google.maps.Map(document.getElementById("map"),options);     
            var marker = new google.maps.Marker({
                    position : sabail,
                    map: map,
                    icon: companyImage,
                    title: 'Our office'  
                }); 
            var marker1 = new google.maps.Marker({
                position : sabail,
                map: map1,
                icon: companyImage,
                title: 'Our office'  
            }); 
            // new map
        }
    </script>



</body>

</html>