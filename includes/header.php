 <!-- Header -->
 <header class="fullscreen-menu">
    <div id="header-container">
    <!-- Logo -->
    <div id="logo" class="hide-ball disable-drag">
        <a class="ajax-link" data-type="page-transition" href="index.html">
            <img class="black-logo" src="images/logo_black.svg" alt="ClaPat Logo">
            <img class="white-logo" src="images/logo_white.svg" alt="ClaPat Logo">
        </a>
    </div>
    <!--/Logo -->
    <!-- Navigation -->
    <nav> 
        <div class="nav-height">
            <div class="outer">
                <div class="inner">           
                    <ul data-breakpoint="10025" class="flexnav">
                        <li class="link menu-timeline"><a class="active" href="index.php" data-type="page-transition"><div class="before-span"><span data-hover="Home">Home</span></div></a></li>
                        <li class="link menu-timeline"><a class="ajax-link" data-type="page-transition" href="index-carousel-columns.php"><div class="before-span"><span data-hover="Magazine">Magazine</span></div></a></li>
                        <li class="link menu-timeline"><a class="ajax-link" data-type="page-transition" href="portfolio-sided-grid.php"><div class="before-span"><span data-hover="Projects">Projects</span></div></a></li>
                        <li class="link menu-timeline"><a class="ajax-link" data-type="page-transition" href="#"><div class="before-span"><span data-hover="News">News</span></div></a></li>
                        <li class="link menu-timeline"><a class="ajax-link" data-type="page-transition" href="about.php"><div class="before-span"><span data-hover="About">About</span></div></a></li>
                        <li class="link menu-timeline"><a class="ajax-link" data-type="page-transition" href="contact.php"><div class="before-span"><span data-hover="Contact">Contact</span></div></a></li>
                        <li class="menu-timeline buy-item"><a target="_blank" class="link " href="https://themeforest.net/user/clapat/portfolio"><div class="before-span"><span data-hover="Buy Now">Buy Now</span></div></a></li>
                    </ul>            
                </div>
            </div> 
        </div>          
    </nav>
    <!--/Navigation -->
    <!-- Menu Burger -->
    <div class="button-wrap right menu  disable-drag">
        <div class="icon-wrap parallax-wrap">
            <div class="button-icon parallax-element">
                <div id="burger-wrapper">
                    <div id="menu-burger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-text sticky right"><span data-hover="Menu">Menu</span></div> 
    </div>
    <!--/Menu Burger -->
    </div>
</header>
<!--/Header -->  