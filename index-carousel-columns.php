<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'code_here', 'auto');
		ga('send', 'pageview');
	</script>
    
    <title>Rayden - Creative Showcase Portfolio Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Download the best Creative Portfolio HTML Template in 2020" />
    <meta name="author" content="ClaPat Studio">
    <meta charset="UTF-8" />    
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
   
</head>


<body class="hidden hidden-ball smooth-scroll" data-primary-color="#E7B970">

	
	<main>		
        <!-- Preloader -->
        <div class="preloader-wrap" data-firstline="Stay" data-secondline="Relaxed">
            <div class="outer">
                <div class="inner">                    
                    <div class="trackbar">
                    	<div class="preloader-intro">Please Wait</div>
                        <div class="loadbar"></div>
                    </div>
                    <div class="percentage-intro">Loaded</div>
                    <div class="percentage-wrapper"><div class="percentage" id="precent"></div></div>                     
                </div>
            </div>
        </div>
        <!--/Preloader -->    
        
        <div class="cd-index cd-main-content">
    
        <!-- Page Content -->
        <div id="page-content" class="light-content" data-bgcolor="#141414">
            
        <?php include 'includes/header.php';?>
            
           
            
            <!-- Content Scroll -->
            <div id="content-scroll">
            
            
                <!-- Main -->
                <div id="main">                
                    
                    <!-- Main Content -->
                    <div id="main-content">
                        
                                                        
                            <!-- Showcase Slider Holder -->
                            <div id="showcase-carousel-holder" class="columns-carousel disable-drag"> 
                                <div id="itemsWrapperLinks">
                                
                                    
                                    <!-- Showcase Slider -->
                                    <div id="showcase-slider" class="swiper-container">
                                        <div id="itemsWrapper" class="swiper-wrapper">
                                        
                                            <div class="swiper-slide wide"> 
                                                <div class="img-mask">
                                                    <div class="section-image">
                                                    	<img src="images/luxe.jpg" class="item-image grid__item-img" alt="">
                                                    </div>                                                
                                                    <img class="grid__item-img grid__item-img--large" src="images/luxe.jpg" />                              
                                                </div>
                                                <a data-type="page-transition" href="project01.php"></a>
                                                <div class="outer">
                                                    <div class="inner">
                                                        <div class="slide-title-wrapper trigger-slide-link" data-firstline="View" data-secondline="Project">
                                                            <div class="slide-title"><span>Luxe</span></div>
                                                        </div>
                                                        <div class="subtitle"><span>Luxe</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="swiper-slide">                                    
                                                <div class="img-mask">
                                                    <div class="section-image">
                                                    	<img class="item-image grid__item-img" src="images/experimental.jpg" alt="">
                                                    </div>                                                
                                                    <img class="grid__item-img grid__item-img--large" src="images/experimental.jpg" />                               
                                                </div>
                                                <a data-type="page-transition" href="project01.php"></a>
                                                <div class="outer">
                                                    <div class="inner">
                                                        <div class="slide-title-wrapper trigger-slide-link" data-firstline="View" data-secondline="Project">
                                                            <div class="slide-title"><span>Experimental</span></div>
                                                        </div>
                                                        <div class="subtitle"><span>Carpets</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="swiper-slide wide">                                    
                                                <div class="img-mask">
                                                    <div class="section-image">
                                                    	<img class="item-image grid__item-img" src="images/pool.jpg" alt="">
                                                    </div>                                                
                                                    <img class="grid__item-img grid__item-img--large" src="images/pool.jpg" />                                    
                                                </div>
                                                <a data-type="page-transition" href="project01.php"></a>
                                                <div class="outer">
                                                    <div class="inner">
                                                        <div class="slide-title-wrapper trigger-slide-link" data-firstline="View" data-secondline="Project">
                                                            <div class="slide-title"><span>Pool</span></div>
                                                        </div>
                                                        <div class="subtitle"><span>pool</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="swiper-slide">                                    
                                                <div class="img-mask">
                                                    <div class="section-image">
                                                    	<img class="item-image grid__item-img" src="images/experimental.jpg" alt="">
                                                    </div>                                                
                                                    <img class="grid__item-img grid__item-img--large" src="images/experimental.jpg" />                                    
                                                </div>
                                                <a data-type="page-transition" href="project01.php"></a>
                                                <div class="outer">
                                                    <div class="inner">
                                                        <div class="slide-title-wrapper trigger-slide-link" data-firstline="View" data-secondline="Project">
                                                            <div class="slide-title"><span>Experimental</span></div>
                                                        </div>
                                                        <div class="subtitle"><span>carpets</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                        </div>                                  
                                    </div>
                                    <!-- Showcase Slider -->                                    
                                    
                                    
                            	</div>                                     
                            </div>    
                            <!-- Showcase Slider Holder --> 
                                         
                                
                    </div>
                    <!--/Main Content --> 
                
                </div>
                <!--/Main -->
                
                <?php include 'includes/footer.php';?>
            
        
        	</div>
            <!--/Content Scroll -->
            
            
            <div id="app"></div>
            
            
		</div>    
        <!--/Page Content -->
    
		</div>
	</main>
    
    
    
    
    <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-drag-x"></div>
            <div id="ball-drag-y"></div>
        	<div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>
    
    
		
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js" ></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpK1sWi3J3EbUOkF_K4-UHzi285HyFX5M&sensor=false"></script>
	<script src="js/clapatwebgl.js"></script>
	<script src="js/plugins.js"></script>
    <script src="js/scripts.js"></script>



</body>

</html>