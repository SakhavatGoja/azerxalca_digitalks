<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'code_here', 'auto');
		ga('send', 'pageview');
	</script>
    
    <title>Rayden - Creative Showcase Portfolio Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Download the best Creative Portfolio HTML Template in 2020" />
    <meta name="author" content="ClaPat Studio">
    <meta charset="UTF-8" />    
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="style.css" rel="stylesheet" />
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/plugin/bootstrap.min.css">
    <link rel="stylesheet" href="css/plugin/bootstrap-grid.css"> -->
</head>
    


<body class="hidden hidden-ball smooth-scroll" data-primary-color="#E7B970">

	
	<main>		
        <!-- Preloader -->
        <div class="preloader-wrap" data-firstline="Stay" data-secondline="Relaxed">
            <div class="outer">
                <div class="inner">                    
                    <div class="trackbar">
                    	<div class="preloader-intro">Please Wait</div>
                        <div class="loadbar"></div>
                    </div>
                    <div class="percentage-intro">Loaded</div>
                    <div class="percentage-wrapper"><div class="percentage" id="precent"></div></div>                     
                </div>
            </div>
        </div>
        <!--/Preloader -->    
        
        <div class="cd-index cd-main-content">
    
        <!-- Page Content -->
        <div id="page-content" class="light-content layered-hero" data-bgcolor="#141414">
            
        <?php include 'includes/header.php';?>       
            
            
             
            
            
            <!-- Content Scroll -->
            <div id="content-scroll">
                <!-- Main -->
                <div id="main">
                    <!-- Main Content -->
                    <div id="main-content">
                          <div class="carpet_inner">
                            <a href="project01.php" class="back_inner">
                              <img src="images/back_arrow.svg" alt="">
                            </a>
                            <div class="right-carpet-content">
                              <div class="carpets_inner_header mobile-show">
                                <div class="carpet_big_title">Multicolor persian hand tufted carpet</div>
                                <div class="carpet_big_price">₼ 3.640</div>
                              </div>
                              <div class="img-inner">
                                <img src="images/carpet_big_img.jpg" alt="">
                              </div>
                              <div class="carpet_inner_info">
                                <div class="carpet_inner_first">
                                  <div class="carpets_inner_header">
                                    <div class="carpet_big_title">Multicolor persian hand tufted carpet</div>
                                    <div class="carpet_big_price">₼ 3.640</div>
                                  </div>
                                  <div class="carpet_inner_content">
                                      A fun warming old Lori rug from the West of Persia.
                                    Around 100 years old, this rug would have been used by the nomadic tribes and 
                                    then traded on for goods they can't produce themselves when it was no longer deemed necessary to keep. 
                                    The Lori tribe has now settled and are no longer a nomadic tribe. This rug is sold as seen. 
                                    However if you wish to purchase this rug in perfect condition please contact info.com for a quote on the price.
                                  </div>
                                  <div class="carpet_inner_size">Size: <span class="carpet_dimensions">2.44m x 1.40m / 8’ x 4’5”</span></div>
                                  <div class="carpet_inner_type">Ref: <span class="carpet_special_type"> BG4133</span></div>
                                  <button class="carpet_order open-modal" id="order_modal">Order</button>
                                </div>
                                <div class="little_carpet_images"><img src="images/little_1.jpg" alt=""><img src="images/little_2.jpg" alt=""></div>
                              </div>
                              <div class="carpets_grid">
                                <div class="carpet_single">
                                    <div class="carpet_img">
                                        <img src="images/carpet_1.jpg" alt="">
                                        <div class="img_shop_box">
                                            <div class="hover_top">
                                                <div class="carpet_hover_name">Northwest Persian</div>
                                                <div class="carpet_hover_price">1.659 AZN</div>
                                                <div class="carpet_hover_dimension">9' 4" x 13' 2" (284cm x 401cm)</div>
                                            </div>
                                            <button>Shop now</button>
                                        </div>
                                    </div>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <div class="carpet_img">
                                        <img src="images/carpet_2.jpg" alt="">
                                        <div class="img_shop_box">
                                            <div class="hover_top">
                                                <div class="carpet_hover_name">Northwest Persian</div>
                                                <div class="carpet_hover_price">1.659 AZN</div>
                                                <div class="carpet_hover_dimension">9' 4" x 13' 2" (284cm x 401cm)</div>
                                            </div>
                                            <button>Shop now</button>
                                        </div>
                                    </div>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                                <div class="carpet_single">
                                    <div class="carpet_img">
                                        <img src="images/carpet_3.jpg" alt="">
                                        <div class="img_shop_box">
                                            <div class="hover_top">
                                                <div class="carpet_hover_name">Northwest Persian</div>
                                                <div class="carpet_hover_price">1.659 AZN</div>
                                                <div class="carpet_hover_dimension">9' 4" x 13' 2" (284cm x 401cm)</div>
                                            </div>
                                            <button>Shop now</button>
                                        </div>
                                    </div>
                                    <div class="carpet-info">
                                        <div class="carpet_name">Multicolor persian hand tufted carpet</div>
                                        <div class="carpet_size">284cm x 401cm</div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                        <!--/Main Page Content --> 
                    </div>
                    <!--/Main Content --> 
                </div>
                <!--/Main --> 

                <!-- Footer -->
                <?php include 'includes/footer.php';?>
                <!--/Footer -->
        	</div>
            <!--/Content Scroll -->
            <div id="app"></div>
           
            <div class="next-project-image-wrapper">
                <div class="next-project-image">
                    <div class="next-project-image-bg" style="background-image:url(images/02hero.jpg)"></div>
                </div>            
            </div>
            
		</div>    
        <!--/Page Content -->
    
		</div>
	</main>
  <div class="modal zero">
    <div class="modal-container zero">
     <button class="close-modal"><img src="images/close.svg" alt=""></button>
      <div class="modal-header">
        <h2>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.</h2></div>
      <div class="modal-content">
        <form action="#" id="carpet_form">
          <input type="text" placeholder="What's Your Name" required>
          <input type="email"  placeholder="What's Your Email" id="" required>
          <input type="number" placeholder="What's Your Number" id="" required>
          <button type="submit" id="order_submit">Order</button>
        </form>
      </div>
    </div>
  </div>

  <div class="modal_success default">
    <div class="modal_success_container default">
      <button class="close_success">
        <img src="images/close.svg" alt="">
      </button>
      <div class="success_content">Thank you for ordering a carpet from us, soon our manager will contact you.</div>
    </div>
  </div>
    
    
    
   <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-drag-x"></div>
            <div id="ball-drag-y"></div>
        	<div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>
    
    
    <script src="js/custom.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js" ></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfqCBbOT1SDziLIysLIVMUQbKHJFqrlEE&sensor=false"></script>
    <script src="js/clapatwebgl.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/scripts.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
    <!-- <script src="js/plugin/bootstrap.min.js"></script>
    <script src="js/plugin/bootstrap.bundle.min.js"></script> -->



</body>

</html>