const openModalBtn = document.querySelectorAll(".open-modal");
const closeModal = document.querySelector(".close-modal");
const modal = document.querySelector(".modal");
const modalContainer = document.querySelector(".modal-container");
const tabs_active_show = document.querySelector('.tabs_active_show');
const mobile_tabs = document.querySelector('.mobile_tabs');

openModalBtn && openModalBtn.forEach((openModal) => {
  openModal.addEventListener("click", () => {
    modal.classList.remove("zero");
    modalContainer.classList.remove("zero");
  });
});

closeModal && closeModal.addEventListener("click", () => {
  setTimeout(() => {
    modal.classList.add("zero");
  },800);
  modalContainer.classList.add("zero");
});

modal && modal.addEventListener("click", (e) => {
  if(e.target.className == "modal"){
     setTimeout(() => {
      modal.classList.add("zero");
    },800);
    modalContainer.classList.add("zero");
  }
  
});

tabs_active_show && tabs_active_show.addEventListener('click', ()=>{
  mobile_tabs.classList.toggle('show');
})