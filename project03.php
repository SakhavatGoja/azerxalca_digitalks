<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'code_here', 'auto');
		ga('send', 'pageview');
	</script>
    
    <title>Rayden - Creative Showcase Portfolio Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Download the best Creative Portfolio HTML Template in 2020" />
    <meta name="author" content="ClaPat Studio">
    <meta charset="UTF-8" />    
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
   
</head>
    


<body class="hidden hidden-ball smooth-scroll" data-primary-color="#E7B970">

	
	<main>		
        <!-- Preloader -->
        <div class="preloader-wrap" data-firstline="Stay" data-secondline="Relaxed">
            <div class="outer">
                <div class="inner">                    
                    <div class="trackbar">
                    	<div class="preloader-intro">Please Wait</div>
                        <div class="loadbar"></div>
                    </div>
                    <div class="percentage-intro">Loaded</div>
                    <div class="percentage-wrapper"><div class="percentage" id="precent"></div></div>                     
                </div>
            </div>
        </div>
        <!--/Preloader -->     
        
        <div class="cd-index cd-main-content">
    
        <!-- Page Content -->
        <div id="page-content" class="light-content" data-bgcolor="#141414">
            
        <?php include 'includes/header.php';?>       
            
            
             
            
            
            <!-- Content Scroll -->
            <div id="content-scroll">
            
            
                <!-- Main -->
                <div id="main">
                
                    <!-- Hero Section -->
                    <div id="hero" class="has-image autoscroll">
                        <div id="hero-styles">
                            <div id="hero-caption" class="reverse-parallax-onscroll">
                                <div class="inner">
                                    <div class="hero-title"><span>Sustainable Fashion</span></div>                                    
                                    <div class="hero-subtitle"><span>The future of textile</span></div>
                                </div>
                            </div>
                            <div id="hero-footer">
                            	<div class="hero-footer-left">
                                	<div class="button-wrap left disable-drag scroll-down">
                                        <div class="icon-wrap parallax-wrap">
                                            <div class="button-icon parallax-element">
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                        </div>
                                        <div class="button-text sticky left"><span data-hover="Scroll or drag to navigate">Scroll or drag to navigate</span></div> 
                                    </div>
                                </div>
                                <div class="hero-footer-right">
                    				<div id="share" class="page-action-content disable-drag" data-text="Share:"></div>
                                </div>
                            </div>                                    
                        </div>
                    </div>
                    <div id="hero-image-wrapper">
                    	<div id="hero-background-layer" class="parallax-scroll-effect">
                            <div id="hero-bg-image" style="background-image:url(images/carpet.jpg)"></div>
                        </div>
                    </div>                        
                    <!--/Hero Section -->    
                         
                    
                    <!-- Main Content -->
                    <div class="project_inner" id="main-content">
                        <div >
                            
                            <!-- Row --> 
                            <div class="vc_row row_padding_top photo_galery">
                                
                                <div class="one_half">                                                        
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet1.jpg" class="image-link"><img src="images/projects/carpet1.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>
                                
                                <div class="one_half last">                                
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet2.jpg" class="image-link"><img src="images/projects/carpet2.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>

                                <div class="one_half">                                                        
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet3.jpg" class="image-link"><img src="images/projects/carpet3.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>
                                
                                <div class="one_half last">                                
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet4.jpg" class="image-link"><img src="images/projects/carpet4.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>
                            
                            </div>
                            <!--/Row -->

                            <!-- Row -->
                            <div class="vc_row project_white change-header-color row_padding_top row_padding_bottom text-align-center">
                                
                                <div class="center_inner_white">
                                    <div class="text-all-info">
                                        <span class="primary-color">10 June 2021, Baku</span>
                                        <h3 class="has-mask">Press release</h3>
                                        <p>
                                            On the occasion of the World Environment Day (5 June), the Embassy of Sweden in Baku launched a preview 
                                            of Sustainable Fashion: The future of textiles exhibition in collaboration with Azerkhalcha OJSC and Stone Chronicle Museum, on 10 June 2021. The exhibition is produced by the Swedish Institute, a public agency that promotes Sweden around the world, together with researchers and fashion experts. It highlights the fashion industry’s major challenges and showcases Swedish solutions and initiatives for a more sustainable future.

                                            The fashion industry is one of the most pollutant industries in the world.  We reuse or recycle only 20 percent 
                                            of 62 million tonnes of clothing that we consume globally per year. The exhibition explains how we can, both as producers and consumers, minimize undesirable environmental effects of the fashion industry by maximizing repairing, remaking, reusing, and recycling of clothing products and their components. It also shows new artistic and curatorial reflections, both on Swedish and Azerbaijani fashion scene, on ecological art and textile production as well as traditional woman handicraft. 

                                            Within the context of the exhibition, newly created design patterns of Azerbaijani carpets are displayed by Azerkhalcha OJSC on a digital format. A colorful digitalization – a personal artistic vision on the future of this naturally pure textile by Azerbaijani artist Orkhan Mammadov creates a completely different design pattern, 
                                            which combines traditional craftmanship and artistic know-how with modern digital trends.  

                                            The exhibition also shows art works of invited artists Ludmila Christeseva (Sweden) and Gunel Ravilova (Azerbaijan). Through a multimedia dialogue, the artists offer two different perspectives on fashion, textiles 
                                            and ecology in two different guises and attempt to identify the role of textiles in the shaping of female identity.
                                            Art curator of the exhibition is Konul Rafiyeva. 

                                            The Sustainable Fashion: The future of textiles exhibition is shown at Stone Chronicles Museum from 11 June 
                                            until 9 July 2021. The entrance is free of charge. 
                                        </p>
                                    </div>
                                </div>
                                
                            </div>
                            <!--/Row -->

                            <div class="vc_row r_top photo_galery">
                                
                                <div class="one_half">                                                        
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet1.jpg" class="image-link"><img src="images/projects/carpet1.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>
                                
                                <div class="one_half last">                                
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet2.jpg" class="image-link"><img src="images/projects/carpet2.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>

                                <div class="one_half">                                                        
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet3.jpg" class="image-link"><img src="images/projects/carpet3.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>
                                
                                <div class="one_half last">                                
                                    <figure class="has-animation" data-delay="0">
                                        <a href="images/projects/carpet4.jpg" class="image-link"><img src="images/projects/carpet4.jpg" alt="Image Title"></a>               
                                    </figure>                                
                                </div>
                            
                            </div>
                            <!--/Row -->
                            
                        
                        </div>
                        <!--/Main Page Content --> 
                    </div>
                    <!--/Main Content --> 
                </div>
                <!--/Main --> 
                
                
                <?php include 'includes/footer.php';?>
            
        
        	</div>
            <!--/Content Scroll -->
            
            <div id="app"></div>
            
            <div class="next-project-image-wrapper">
                <div class="next-project-image">
                    <div class="next-project-image-bg" style="background-image:url(images/04hero.jpg)"></div>
                </div>            
            </div>            
            
		</div>    
        <!--/Page Content -->
    
		</div>
	</main>
    
    
    
    
    <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-drag-x"></div>
            <div id="ball-drag-y"></div>
        	<div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>
    
    
		
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js" ></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpK1sWi3J3EbUOkF_K4-UHzi285HyFX5M&sensor=false"></script>
	<script src="js/clapatwebgl.js"></script>
	<script src="js/plugins.js"></script>
    <script src="js/scripts.js"></script>



</body>

</html>